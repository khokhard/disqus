var express = require('express');
require('./../public/constants');
var router = express.Router();

const request = require('request');
/* GET forum post listing. */
router.get('/getForumPosts', function(req, res, next) {
   
 request(apiurl+'/forums/listPosts.json?forum=disqus&api_key='+apikey+'&api_secret='+apisecret, function(err, response, body) {  
    let json = JSON.parse(body);
     res.send(json);
});
});
router.get('/userdetail', function(req, res, next) {
 
  request(apiurl+'/users/details.json?user=1&api_key='+apikey+'&api_secret='+apisecret, function(err, response, body) {  
    let json = JSON.parse(body);
     res.send(json);
});
 
});
router.get('/checkuser', function(req, res, next) {
 
  console.log(req.body);
 
});

router.get('/interestingUsers', function(req, res, next) {
 
  request(apiurl+'/users/interestingUsers.json?api_key='+apikey+'&api_secret='+apisecret, function(err, response, body) {  
    let json = JSON.parse(body);
     res.send(json);
});
 
});
router.get('/authenticate', function(req, res, next) {
 
  request('https://disqus.com/api/oauth/2.0/authorize/?client_id='+apikey+'&api_secret='+apisecret+'&scope=read,write&response_type=code', function(err, response, body) {  
  //  let json = JSON.parse(body);
    //console.log(body);
     res.send(body);
});
 
});

router.get('/moderatedChannels', function(req, res, next) {
 
  request(apiurl+'/users/listModeratedChannels.json?user=1&api_key='+apikey+'&api_secret='+apisecret, function(err, response, body) {  
  //  let json = JSON.parse(body);
    //console.log(body);
     res.send(body);
});
 
});
module.exports = router;
